Кратко о проекте:
  Загружает из API https://api.tinkoff.ru/v1/news заголовки новостей банка.
  Кэширует в локальную бд: инсертятся все пришедшие записи, дата публикации которых больше, чем дата публикации самой последней закэшированной новости.
  Отображает в виде списка - текст и дата публикации в верхнем правом углу.

  При клике по заголовку новости открывается содержимое этой новости.
  Содержимое загружается из API https://api.tinkoff.ru/v1/news_content?id=[номер новости].
  Кэширует содержимое в локальную бд: инсертит если записи не было, или апдейтит если дата последней модификации пришедшего содержимого больше даты последней модификации закэшированного.
  Отображается в WebView, растянутой на весь экран.

Примечание:
  Как и говорилось в ТЗ, не использовал сторонних библиотек на подобии dagger 2, mosby и тд. Некоторые вещи раньше наживую не делал, т.ч. вполне вероятно запроектировал архитектуру не самым лучшим способом.
  Чтение из бд идёт в ui потоке.
  Обращение к серверу, получение ответа и кэширование НЕ в ui потоке.
  Для асинхронной работы использую AsyncTask, который запускаю из ретейн фрагмента, который по завершению работы асинк такска сразу вернут результат активити, если она есть, или вернёт его как только активити появится. (т.е. взял пример, который нам показали на курсах финтех школы)
  Если приложения запустят без подключения к интернету, будут отображаться закэшированные данные.

Что ещё хотелось бы сделать (попробую завтра вечером сделать, но это уже дело времени):
  Сделать постепенную подгрузку списка заголовков новостей(!). Т.е. догружать список визуально при свайпе вверх, когда доходят до конца. Сейчас же отображаются в обще все элементы.
  Добавить читаемое сообщение о том, что связи с интернетом нет.
  Протестить на наличие ошибок. Причесать архитектуру. Вынести захардкоженные сообщения в strings.
  Мб что-то ещё вспомню завтра.