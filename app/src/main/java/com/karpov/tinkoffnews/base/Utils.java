package com.karpov.tinkoffnews.base;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static void Log(String text) {
        Log.d("happy", text);
    }

    public static void showMessage(String text, Context context) {
        Toast.makeText(
                context,
                text,
                (text.length() > 20 ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)
        ).show();
    }

    public static String dateToStr(Date date) {
        if (date == null)
            return "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy"); // формат подсмотрел на сайте новостей Тинькоффа - просто день и год (буковку "г." дописывать не хочу)
        return dateFormat.format(date);
    }

    public static long getDateByJsonString(String jsonString) throws Exception { // самодельный костыль для распарсивания Тинькоффских дат (времени на исследование как это сделать грамотно не хватает, мб позже)
        JSONObject jSONObject = new JSONObject(jsonString);
        if (!jSONObject.isNull("milliseconds"))
            return jSONObject.getLong("milliseconds");
        else
            return 0;
    }
}
