package com.karpov.tinkoffnews.base.base_getter;

public class BaseGetterTaskResult {
    private final Boolean result;
    private final String errorMessage;

    public BaseGetterTaskResult(Boolean result, String errorMessage) {
        this.result = result;
        this.errorMessage = errorMessage;
    }

    public Boolean getResult() {
        return result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
