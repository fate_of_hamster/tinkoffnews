package com.karpov.tinkoffnews.base;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ManagerNetwork {

    private static final String URL_GET_NEWS = "https://api.tinkoff.ru/v1/news";
    private static final String URL_GET_NEWS_CONTENT = "https://api.tinkoff.ru/v1/news_content?id="; // к этому url нужно прибавить newsID в конец, что бы он стал рабочим


    private static ManagerNetwork instance;

    public static ManagerNetwork getInstance() {
        if (instance == null)
            instance = new ManagerNetwork();
        return instance;
    }


    public String getNews() throws Exception {
        return getData(URL_GET_NEWS);
    }

    public String getNewsContent(int newsID) throws Exception {
        return getData(URL_GET_NEWS_CONTENT + newsID);
    }


    /*
    Метод получает url, выполняет по указанному url GET запрос, возвращает payload в виде строки.
    (!) Это тяжёлый метод, запускать НЕ в ui потоке (!)
    Все строки с сообщениями хороше бы вынести в Strings, но пока не буду этого делать.
     */
    private String getData(String urlString) throws Exception {
        Utils.Log("ManagerNetwork: getData:"
                + "\n  start GET to urlString = [" + urlString + "]");

        String resultString;
        String payloadString = null;
        int responseCode;

        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            responseCode = urlConnection.getResponseCode();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = bufferedReader.readLine()) != null) {
                response.append(inputLine);
            }
            bufferedReader.close();
            resultString = response.toString();

            JSONObject data = new JSONObject(resultString);
            if (!data.isNull("resultCode")){
                String resultCode = data.getString("resultCode");
                if (!resultCode.equals("OK")) {
                    throw new Exception("Ошибка запроса к серверу. Код ответа: " + resultCode);
                }
            }
            if (!data.isNull("payload")) {
                payloadString = data.getString("payload");
            }
        }
        catch (MalformedURLException e) {
            throw new Exception("MalformedURLException: " + e.getMessage());
        }
        catch (UnsupportedEncodingException e) {
            throw new Exception("UnsupportedEncodingException: " + e.getMessage());
        }
        catch (FileNotFoundException e) {
            throw new Exception("FileNotFoundException: " + e.getMessage());
        }
        catch (Exception e) {
            throw new Exception("Exception: " + e.getMessage());
        }

        Utils.Log("ManagerNetwork: getData:"
                + "\n  urlString     = [" + urlString + "]"
                + "\n  responseCode  = [" + responseCode + "]"
                + "\n  resultString  = [" + resultString + "]"
                + "\n  payloadString = [" + payloadString + "]");
        return payloadString;
    }
}
