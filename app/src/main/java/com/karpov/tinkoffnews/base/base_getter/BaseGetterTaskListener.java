package com.karpov.tinkoffnews.base.base_getter;

public interface BaseGetterTaskListener {
    void onLoadStart();
    void onLoadError(String errorMessage);
    void onLoadFinish(Boolean result);
}
