package com.karpov.tinkoffnews.base;

import android.app.Application;

public class MyApp extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        ManagerDb.initInstance(getApplicationContext());
    }
}
