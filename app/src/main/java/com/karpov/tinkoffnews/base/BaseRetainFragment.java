package com.karpov.tinkoffnews.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTaskListener;

public abstract class BaseRetainFragment extends Fragment implements BaseGetterTaskListener {

    private BaseGetterTaskListener listener;
    private Boolean bufResult;
    private boolean isLoadingInProcess;

    public abstract void startGetData(Object params);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        this.isLoadingInProcess = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseGetterTaskListener) {
            this.listener = (BaseGetterTaskListener) context;
        }
        pushResultToListener();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void pushResultToListener() {
        if (this.listener != null) {
            if (bufResult != null) {
                this.listener.onLoadFinish(bufResult);
                bufResult = null;
            } else if (isLoadingInProcess) {
                this.listener.onLoadStart();
            }
        }
    }

    @Override
    public void onLoadStart() {
        this.isLoadingInProcess = true;
        if (this.listener != null) {
            this.listener.onLoadStart();
        }
    }

    @Override
    public void onLoadError(String errorMessage) {
        if (this.listener != null)
            this.listener.onLoadError(errorMessage);
    }

    @Override
    public void onLoadFinish(Boolean result) {
        this.isLoadingInProcess = false;
        if (this.listener != null)
            this.listener.onLoadFinish(result);
        else
            this.bufResult = result;

    }
}
