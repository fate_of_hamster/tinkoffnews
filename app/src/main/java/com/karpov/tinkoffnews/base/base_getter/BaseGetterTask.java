package com.karpov.tinkoffnews.base.base_getter;

import android.os.AsyncTask;

public abstract class BaseGetterTask extends AsyncTask<String, Void, BaseGetterTaskResult> {

    private BaseGetterTaskListener listener;

    public BaseGetterTask(BaseGetterTaskListener listener){
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null)
            listener.onLoadStart();
    }

    @Override
    protected BaseGetterTaskResult doInBackground(String... params) {
        return doHardWork(params);
    }

    @Override
    protected void onPostExecute(BaseGetterTaskResult result) {
        if (listener != null) {
            if (result.getErrorMessage() != null)
                listener.onLoadError(result.getErrorMessage());
            else
                listener.onLoadFinish(result.getResult());
        }
    }

    public abstract BaseGetterTaskResult doHardWork(String... params);
}
