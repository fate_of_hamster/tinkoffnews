package com.karpov.tinkoffnews.base;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.karpov.tinkoffnews.storage.NewsContentItem;
import com.karpov.tinkoffnews.storage.NewsItem;
import java.util.ArrayList;
import java.util.List;

/*
От автора: я впервые работаю масштабно с чистым SQLite, так что наверняка что нибудь сделаю плохо
 */

public class ManagerDb extends SQLiteOpenHelper {

    private static ManagerDb instance;

    public static final String DB_NAME = "TinkoffNewsSQLiteDB";
    public static final int DB_VERSION = 4;

    public static ManagerDb initInstance(Context context) {
        if (instance == null) {
            instance = new ManagerDb(context);
        }
        return instance;
    }
    public static ManagerDb getInstance() {
        return instance;
    }
    private ManagerDb(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(NewsItem.EXEC_TABLE_CREATE);
        db.execSQL(NewsContentItem.EXEC_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { // пока упрощаем, просто дропаем текщие таблицы и создаём всё по новой
        db.execSQL(NewsItem.EXEC_TABLE_DROP);
        db.execSQL(NewsItem.EXEC_TABLE_CREATE);

        db.execSQL(NewsContentItem.EXEC_TABLE_DROP);
        db.execSQL(NewsContentItem.EXEC_TABLE_CREATE);
    }

    //-------------------------------------------------------------------------------------- новости
    // добавить новые новости (возвращает кол-во добавленных новостей)
    public int addNewNews(List<NewsItem> items) {
        int result = 0;
        long localMaxPubDate = getNewsMaxPublicationDate(); // дата публикации самой свежей новости из локальной бд
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            for (NewsItem newsItem : items) {
                if (newsItem.getPublicationDate() > localMaxPubDate) { // добавялем только если дата публикации новее даты публикации самой свежей новости из локальной бд
                    db.insert(NewsItem.TABLE_NAME, null, newsItem.toContentValues());
                    result++;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return result;
    }

    // получить список всех новостей
    public List<NewsItem> getAllNews() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                NewsItem.TABLE_NAME
                ,NewsItem.getAllFieldNames()
                ,null
                ,null
                ,null
                ,null
                ,NewsItem.FIELD_PUBLICATION_DATE + " Desc"
        );

        List<NewsItem> items = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                items.add(new NewsItem(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return items;
    }

    // дата публикации самой свежей новости из локальной бд
    public long getNewsMaxPublicationDate() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                NewsItem.TABLE_NAME
                ,new String[] {NewsItem.FIELD_PUBLICATION_DATE}
                ,null
                ,null
                ,null
                ,null
                ,NewsItem.FIELD_PUBLICATION_DATE + " Desc"
                ,"1"
        );
        long publicationDate = 0;
        if (cursor.moveToFirst())
            publicationDate = cursor.getLong(cursor.getColumnIndexOrThrow(NewsItem.FIELD_PUBLICATION_DATE));
        cursor.close();

        return publicationDate;
    }

    // взять одну новость по её id
    public NewsItem getNews(int newsID) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                NewsItem.TABLE_NAME
                ,NewsItem.getAllFieldNames()
                ,NewsItem.FIELD_ID + " like ?"
                , new String[] {"" + newsID}
                ,null
                ,null
                ,null
                ,"1"
        );

        NewsItem item = null;
        if (cursor.moveToFirst())
            item = new NewsItem(cursor);
        cursor.close();

        return item;
    }


    //--------------------------------------------------------------------------- содержание новости
    // взять содержание одной новости по её id
    public NewsContentItem getNewsContent(int newsID) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                NewsContentItem.TABLE_NAME
                ,NewsContentItem.getAllFieldNames()
                ,NewsContentItem.FIELD_NEWS_ID + " like ?"
                ,new String[] {"" + newsID}
                ,null
                ,null
                ,null
                ,"1"
        );
        NewsContentItem item = null;
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst())
                item = new NewsContentItem(cursor);
            cursor.close();
        }
        return item;
    }

    // сохранить содержание одной новости
    public boolean saveNewsContent(NewsContentItem item) {
        NewsContentItem localItem = getNewsContent(item.getNewsId());
        if (localItem == null) {
            insertNewsContent(item);
            return true;
        }
        else if (localItem.getLastModificationDate() < item.getLastModificationDate()) {
            updateNewsContent(item);
            return true;
        }
        else
            return false;
    }

    public void insertNewsContent(NewsContentItem item) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            db.insert(NewsContentItem.TABLE_NAME, null, item.toContentValues());
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void updateNewsContent(NewsContentItem item) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            db.update(NewsContentItem.TABLE_NAME
                    ,item.toContentValues()
                    ,NewsContentItem.FIELD_NEWS_ID + " like ?"
                    ,new String[] {"" + item.getNewsId()}
            );
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}
