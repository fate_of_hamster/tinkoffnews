package com.karpov.tinkoffnews.news_content;

import android.os.AsyncTask;
import com.karpov.tinkoffnews.base.ManagerDb;
import com.karpov.tinkoffnews.base.ManagerNetwork;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTask;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTaskListener;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTaskResult;
import com.karpov.tinkoffnews.storage.NewsContentItem;

public class NewsContentGetterTask extends BaseGetterTask {

    private ManagerDb dbManager;
    private ManagerNetwork networkManager;

    public static void start(BaseGetterTaskListener listener, int newsID) {
        listener.onLoadStart();
        (new NewsContentGetterTask(listener)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "" + newsID);
    }

    public NewsContentGetterTask(BaseGetterTaskListener listener) {
        super(listener);
        dbManager = ManagerDb.getInstance();
        networkManager = ManagerNetwork.getInstance();
    }

    @Override
    public BaseGetterTaskResult doHardWork(String... params) {
        String errorMessage  = null;
        NewsContentItem item = null;
        Boolean result = false;
        int newsID = 0;

        if ((params.length > 0)
                && (params[0] != null))
            newsID = Integer.parseInt(params[0]);

        if (newsID == 0) {
            errorMessage = "Не найдена ссылка на новость.";
        }
        else {
            try {
                String jsonString = networkManager.getNewsContent(newsID); // запрос новостей с сервера
                item = new NewsContentItem(jsonString);
            } catch (Exception e) {
                errorMessage = e.getMessage();
            }
            result = (item != null)
                    && dbManager.saveNewsContent(item); // вернём true, если какие то записи добавились в локальную бд
        }

        return new BaseGetterTaskResult(result, errorMessage);
    }
}
