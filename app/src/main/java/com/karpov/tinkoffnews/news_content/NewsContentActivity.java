package com.karpov.tinkoffnews.news_content;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import com.karpov.tinkoffnews.R;
import com.karpov.tinkoffnews.base.ManagerDb;
import com.karpov.tinkoffnews.base.Utils;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTaskListener;
import com.karpov.tinkoffnews.storage.NewsContentItem;
import com.karpov.tinkoffnews.storage.NewsItem;

public class NewsContentActivity extends AppCompatActivity  implements BaseGetterTaskListener,  SwipeRefreshLayout.OnRefreshListener {

    private static final String NEWS_ID = "NEWS_ID";
    private int newsID;
    private NewsContentRetainFragment retainFragment;
    private SwipeRefreshLayout swipeRefresh;
    private WebView webViewMain;
    private ManagerDb dbManager;

    public static void start(int newsID, Context context) {
        Intent intent = new Intent(context, NewsContentActivity.class);
        intent.putExtra(NEWS_ID, newsID);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_content);
        dbManager = ManagerDb.getInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            newsID = bundle.getInt(NEWS_ID, 0);
        }

        if (savedInstanceState != null) {
            retainFragment = (NewsContentRetainFragment) getSupportFragmentManager().findFragmentByTag(NewsContentRetainFragment.TAG);
        }
        if (retainFragment == null) {
            retainFragment = new NewsContentRetainFragment();
            getSupportFragmentManager().beginTransaction().add(retainFragment, NewsContentRetainFragment.TAG).commit();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swp_swipe_refresh);
        swipeRefresh.setOnRefreshListener(this);

        webViewMain = (WebView) findViewById(R.id.wv_web_view_main);

        if (savedInstanceState != null)
            refreshFromLocalBD();
        else
            refreshFromServer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshFromLocalBD();
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoadStart() {
        swipeRefresh.setRefreshing(true);
    }

    @Override
    public void onLoadError(String errorMessage) {
        Utils.showMessage(errorMessage, this);
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onLoadFinish(Boolean result) {
        if (result)
            refreshFromLocalBD();
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        refreshFromServer();
    }

    private void refreshFromServer() {
        retainFragment.startGetData(newsID);
    }

    private void refreshFromLocalBD() {
        NewsItem newsItem = dbManager.getNews(newsID);
        if (newsItem != null) {
            getSupportActionBar().setTitle(newsItem.getText());
            getSupportActionBar().setSubtitle(newsItem.getPublicationDateToDisplay());
        }

        NewsContentItem newsContentItem = dbManager.getNewsContent(newsID);
        if (newsContentItem != null)
            webViewMain.loadData(newsContentItem.getContent(), "text/html; charset=UTF-8", null);
    }
}
