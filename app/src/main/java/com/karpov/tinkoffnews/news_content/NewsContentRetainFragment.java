package com.karpov.tinkoffnews.news_content;

import com.karpov.tinkoffnews.base.BaseRetainFragment;

public class NewsContentRetainFragment extends BaseRetainFragment {

    public static final String TAG = "NewsContentRetainFragment";

    @Override
    public void startGetData(Object params) {
        if (params != null)
            NewsContentGetterTask.start(this, (int) params);
    }
}
