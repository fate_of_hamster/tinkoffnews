package com.karpov.tinkoffnews.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.karpov.tinkoffnews.R;
import com.karpov.tinkoffnews.base.Utils;
import com.karpov.tinkoffnews.storage.NewsItem;

import java.util.Date;
import java.util.List;

public class NewsDictAdapter extends RecyclerView.Adapter<NewsDictAdapter.ViewHolder> {

    private List<NewsItem> items;
    private OnNewsDictClickListener clickListener;

    public NewsDictAdapter(OnNewsDictClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public NewsDictAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_dict_item, parent, false);
        return new ViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.publicationDate.setText(items.get(position).getPublicationDateToDisplay());
        holder.text.setText(items.get(position).getText());
    }

    @Override
    public int getItemCount() {
        if (items == null)
            return 0;
        else
            return items.size();
    }


    public void setItems(List<NewsItem> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }
    public NewsItem getItemByPosition(int position) {
        return items.get(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text;
        public TextView publicationDate;

        public ViewHolder(View view, OnNewsDictClickListener listener) {
            super(view);
            text = (TextView) view.findViewById(R.id.lbl_text);
            publicationDate = (TextView) view.findViewById(R.id.lbl_publication_date);
            setListener(listener);
        }

        private void setListener(final OnNewsDictClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onClick(getAdapterPosition());
                }
            });
        }
    }
}