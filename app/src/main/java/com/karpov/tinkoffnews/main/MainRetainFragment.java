package com.karpov.tinkoffnews.main;

import com.karpov.tinkoffnews.base.BaseRetainFragment;

public class MainRetainFragment extends BaseRetainFragment {

    public static final String TAG = "MainRetainFragment";

    @Override
    public void startGetData(Object params) {
        NewsGetterTask.start(this);
    }
}
