package com.karpov.tinkoffnews.main;

import android.os.AsyncTask;

import com.karpov.tinkoffnews.base.ManagerDb;
import com.karpov.tinkoffnews.base.ManagerNetwork;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTaskListener;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTask;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTaskResult;
import com.karpov.tinkoffnews.storage.NewsItem;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;

public class NewsGetterTask extends BaseGetterTask {

    private ManagerDb dbManager;
    private ManagerNetwork networkManager;

    public static void start(BaseGetterTaskListener listener) {
        listener.onLoadStart();
        (new NewsGetterTask(listener)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public NewsGetterTask(BaseGetterTaskListener listener) {
        super(listener);
        dbManager = ManagerDb.getInstance();
        networkManager = ManagerNetwork.getInstance();
    }

    @Override
    public BaseGetterTaskResult doHardWork(String... params) {
        String errorMessage  = null;

        List<NewsItem> items = new ArrayList<>();
        try {
            String jsonString = networkManager.getNews(); // запрос новостей с сервера (я не нашёл как попросить сервер дать мне не все овости, а только новые; так что запрашиваю все)

            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                NewsItem item = new NewsItem(jsonArray.getString(i));
                items.add(item);
            }
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        Boolean result = (dbManager.addNewNews(items) > 0); // вернём true, если какие то записи добавились в локальную бд

        return new BaseGetterTaskResult(result, errorMessage);
    }
}
