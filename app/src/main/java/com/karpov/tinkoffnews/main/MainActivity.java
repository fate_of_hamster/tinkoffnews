package com.karpov.tinkoffnews.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.karpov.tinkoffnews.base.ManagerDb;
import com.karpov.tinkoffnews.base.Utils;
import com.karpov.tinkoffnews.base.base_getter.BaseGetterTaskListener;
import com.karpov.tinkoffnews.R;
import com.karpov.tinkoffnews.news_content.NewsContentActivity;

public class MainActivity extends AppCompatActivity implements BaseGetterTaskListener,  SwipeRefreshLayout.OnRefreshListener {

    private MainRetainFragment retainFragment;
    private RecyclerView recyclerView;
    private NewsDictAdapter adapter;
    private SwipeRefreshLayout swipeRefresh;
    private ManagerDb dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbManager = ManagerDb.getInstance();

        if (savedInstanceState != null) {
            retainFragment = (MainRetainFragment) getSupportFragmentManager().findFragmentByTag(MainRetainFragment.TAG);
        }
        if (retainFragment == null) {
            retainFragment = new MainRetainFragment();
            getSupportFragmentManager().beginTransaction().add(retainFragment, MainRetainFragment.TAG).commit();
        }

        initRecyclerView();

        if (savedInstanceState != null)
            refreshFromLocalBD();
        else
            refreshFromServer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshFromLocalBD();
    }

    private void initRecyclerView() {
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swp_swipe_refresh);
        swipeRefresh.setOnRefreshListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.lst_news_dict);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        OnNewsDictClickListener onRoomDictClickListener = new OnNewsDictClickListener() {
            @Override
            public void onClick(int position) {
                adapter.getItemByPosition(position).getId();
                NewsContentActivity.start(adapter.getItemByPosition(position).getId(), MainActivity.this);
            }
        };
        adapter = new NewsDictAdapter(onRoomDictClickListener);
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onLoadStart() {
        swipeRefresh.setRefreshing(true);
    }

    @Override
    public void onLoadError(String errorMessage) {
        Utils.showMessage(errorMessage, this);
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onLoadFinish(Boolean result) {
        if (result)
            refreshFromLocalBD();
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        refreshFromServer();
    }

    private void refreshFromServer() {
        retainFragment.startGetData(null);
    }

    private void refreshFromLocalBD() {
        adapter.setItems(dbManager.getAllNews()); // дадим себе слабину, начитаем новости в ui потоке
    }
}
