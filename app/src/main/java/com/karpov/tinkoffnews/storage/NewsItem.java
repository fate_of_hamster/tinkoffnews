package com.karpov.tinkoffnews.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.Html;
import com.karpov.tinkoffnews.base.Utils;

import org.json.JSONObject;

import java.util.Date;

public class NewsItem {

    private int id;
    private String name;
    private String text;
    private long publicationDate;
    private int bankInfoTypeId;

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_TEXT = "text";
    public static final String FIELD_PUBLICATION_DATE = "publicationDate";
    public static final String FIELD_BANK_INFO_TYPE_ID = "bankInfoTypeId";

    public static final String TABLE_NAME = "news";
    public static final String EXEC_TABLE_CREATE =
            "create table " + TABLE_NAME + " ("
                    + NewsItem.FIELD_ID + " integer primary key"
                    + "," + NewsItem.FIELD_NAME + " text"
                    + "," + NewsItem.FIELD_TEXT + " text"
                    + "," + NewsItem.FIELD_PUBLICATION_DATE + " long"
                    + "," + NewsItem.FIELD_BANK_INFO_TYPE_ID + " integer"
                    + ")";
    public static final String EXEC_TABLE_DROP = "drop table if exists " + TABLE_NAME;

    public NewsItem(int id, String name, String text, long publicationDate, int bankInfoTypeId) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.publicationDate = publicationDate;
        this.bankInfoTypeId = bankInfoTypeId;
    }

    public static String[] getAllFieldNames() {
        return new String[] {NewsItem.FIELD_ID
                ,NewsItem.FIELD_NAME
                ,NewsItem.FIELD_TEXT
                ,NewsItem.FIELD_PUBLICATION_DATE
                ,NewsItem.FIELD_BANK_INFO_TYPE_ID};
    }

    public NewsItem(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndexOrThrow(NewsItem.FIELD_ID));
        this.name = cursor.getString(cursor.getColumnIndexOrThrow(NewsItem.FIELD_NAME));
        this.text = cursor.getString(cursor.getColumnIndexOrThrow(NewsItem.FIELD_TEXT));
        this.publicationDate = cursor.getLong(cursor.getColumnIndexOrThrow(NewsItem.FIELD_PUBLICATION_DATE));
        this.bankInfoTypeId = cursor.getInt(cursor.getColumnIndexOrThrow(NewsItem.FIELD_BANK_INFO_TYPE_ID));
    }

    public NewsItem(String jsonString)  throws Exception {
        JSONObject jsonObj = new JSONObject(jsonString);

        if (!jsonObj.isNull(FIELD_ID))
            this.id = jsonObj.getInt(FIELD_ID);

        if (!jsonObj.isNull(FIELD_NAME)) {
            this.name = jsonObj.getString(FIELD_NAME);
        }

        if (!jsonObj.isNull(FIELD_TEXT)) {
            this.text = Html.fromHtml(jsonObj.getString(FIELD_TEXT)).toString(); // буду созранять просто текст, без html, т.к. не увидел в html чего-то важного
        }

        if (!jsonObj.isNull(FIELD_PUBLICATION_DATE)) {
            this.publicationDate = Utils.getDateByJsonString(jsonObj.getString(FIELD_PUBLICATION_DATE));
        }

        if (!jsonObj.isNull(FIELD_BANK_INFO_TYPE_ID)) {
            this.bankInfoTypeId = jsonObj.getInt(FIELD_BANK_INFO_TYPE_ID);
        }
    }


    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIELD_ID, this.id);
        contentValues.put(FIELD_NAME, this.name);
        contentValues.put(FIELD_TEXT, this.text);
        contentValues.put(FIELD_PUBLICATION_DATE, this.publicationDate);
        contentValues.put(FIELD_BANK_INFO_TYPE_ID, this.bankInfoTypeId);
        return contentValues;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public long getPublicationDate() {
        return publicationDate;
    }
    public String getPublicationDateToDisplay() {
        return Utils.dateToStr(new Date(publicationDate));
    }

    public int getBankInfoTypeId() {
        return bankInfoTypeId;
    }
}
