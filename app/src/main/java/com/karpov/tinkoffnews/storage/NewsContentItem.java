package com.karpov.tinkoffnews.storage;

import android.content.ContentValues;
import android.database.Cursor;

import com.karpov.tinkoffnews.base.Utils;
import org.json.JSONObject;

public class NewsContentItem {

    private int newsID;
    private long creationDate;
    private long lastModificationDate;
    private String content;
    private int bankInfoTypeId;
    private String typeId;

    public static final String FIELD_NEWS_ID = "newsID";
    public static final String FIELD_CREATION_DATE = "creationDate";
    public static final String FIELD_LAST_MODIFICATION_DATE = "lastModificationDate";
    public static final String FIELD_CONTENT = "content";
    public static final String FIELD_BANK_INFO_TYPE_ID = "bankInfoTypeId";
    public static final String FIELD_TYPE_ID = "typeId";

    public static final String TABLE_NAME = "newsContent";
    public static final String EXEC_TABLE_CREATE =
            "create table " + TABLE_NAME + " ("
                    + FIELD_NEWS_ID + " integer primary key"
                    + "," + FIELD_CREATION_DATE + " long"
                    + "," + FIELD_LAST_MODIFICATION_DATE + " long"
                    + "," + FIELD_CONTENT + " text"
                    + "," + FIELD_BANK_INFO_TYPE_ID + " integer"
                    + "," + FIELD_TYPE_ID + " text"
                    + ")";
    public static final String EXEC_TABLE_DROP = "drop table if exists " + TABLE_NAME;

    public NewsContentItem(int newsID, long creationDate, long lastModificationDate, String content, int bankInfoTypeId, String typeId) {
        this.newsID = newsID;
        this.creationDate = creationDate;
        this.lastModificationDate = lastModificationDate;
        this.content = content;
        this.bankInfoTypeId = bankInfoTypeId;
        this.typeId = typeId;
    }

    public static String[] getAllFieldNames() {
        return new String[] {FIELD_NEWS_ID
                ,FIELD_CREATION_DATE
                ,FIELD_LAST_MODIFICATION_DATE
                ,FIELD_CONTENT
                ,FIELD_BANK_INFO_TYPE_ID
                ,FIELD_TYPE_ID};
    }

    public NewsContentItem(Cursor cursor) {
        this.newsID = cursor.getInt(cursor.getColumnIndexOrThrow(FIELD_NEWS_ID));
        this.creationDate = cursor.getLong(cursor.getColumnIndexOrThrow(FIELD_CREATION_DATE));
        this.lastModificationDate = cursor.getLong(cursor.getColumnIndexOrThrow(FIELD_LAST_MODIFICATION_DATE));
        this.content = cursor.getString(cursor.getColumnIndexOrThrow(FIELD_CONTENT));
        this.bankInfoTypeId = cursor.getInt(cursor.getColumnIndexOrThrow(FIELD_BANK_INFO_TYPE_ID));
        this.typeId = cursor.getString(cursor.getColumnIndexOrThrow(FIELD_TYPE_ID));
    }

    public NewsContentItem(String jsonString)  throws Exception {
        JSONObject jsonObj = new JSONObject(jsonString);

        if (!jsonObj.isNull("title")) {
            JSONObject jsonObjTitle = new JSONObject(jsonObj.getString("title"));
            if (!jsonObjTitle.isNull("id"))
                this.newsID = jsonObjTitle.getInt("id");
        }

        if (!jsonObj.isNull(FIELD_CREATION_DATE))
            this.creationDate = Utils.getDateByJsonString(jsonObj.getString(FIELD_CREATION_DATE));

        if (!jsonObj.isNull(FIELD_LAST_MODIFICATION_DATE))
            this.lastModificationDate = Utils.getDateByJsonString(jsonObj.getString(FIELD_LAST_MODIFICATION_DATE));

        if (!jsonObj.isNull(FIELD_CONTENT))
            this.content = jsonObj.getString(FIELD_CONTENT);

        if (!jsonObj.isNull(FIELD_BANK_INFO_TYPE_ID))
            this.bankInfoTypeId = jsonObj.getInt(FIELD_BANK_INFO_TYPE_ID);

        if (!jsonObj.isNull(FIELD_TYPE_ID))
            this.typeId = jsonObj.getString(FIELD_TYPE_ID);
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIELD_NEWS_ID, this.newsID);
        contentValues.put(FIELD_CREATION_DATE, this.creationDate);
        contentValues.put(FIELD_LAST_MODIFICATION_DATE, this.lastModificationDate);
        contentValues.put(FIELD_CONTENT, this.content);
        contentValues.put(FIELD_BANK_INFO_TYPE_ID, this.bankInfoTypeId);
        contentValues.put(FIELD_TYPE_ID, this.typeId);
        return contentValues;
    }

    public int getNewsId() {
        return newsID;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public long getLastModificationDate() {
        return lastModificationDate;
    }

    public String getContent() {
        return content;
    }

    public int getBankInfoTypeId() {
        return bankInfoTypeId;
    }

    public String getTypeId() {
        return typeId;
    }
}
